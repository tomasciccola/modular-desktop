module.exports.fetchJson = (url, opts, cb) => {
  if(typeof opts === 'object'){
    fetch(url, opts).then(res => res.json()).then(d => cb(null,d)).catch(e => cb(e,null))
  }else if(typeof opts === 'function'){
    let c = opts
    fetch(url).then(res => res.json()).then(d => c(null,d)).catch(e => c(e,null))
  }
}
const url = require('url')
const path = require('path')
module.exports.toSafeFileName = n => path.basename(url.parse(n).pathname)
