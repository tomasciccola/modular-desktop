const Nanocomponent = require('nanocomponent')
const html = require('choo/html')

class Footer extends Nanocomponent {
  constructor(){
    super()
  }
  createElement() {
    return html`
      <p class="flex flex-row justify-center items-center mb4 h-25"> 
        © ${(new Date()).getUTCFullYear()}
        <span class="b ml2">SRZ</span>
      </p>
    `
  }

  update() {
    return false
  }

}
module.exports = Footer
