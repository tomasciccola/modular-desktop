const Nanocomponent = require('nanocomponent')
const html = require('choo/html')
const Toggle = require('./Toggle.js')
const toggle = new Toggle()

class Player extends Nanocomponent {
  constructor() {
    super()
    this.size = `fa-3x`
    this.buttonStyle = `bg-white b--none pa3`
    this.playOrPause = this.playOrPause.bind(this)
    this.next = this.next.bind(this)
  }

  playOrPause () {
    let player = this.state.players[this.state.atPlayer]
    player.playing 
      ? this.emit('track:pause')
      : this.emit('track:play', false)
  } 

  next() {
    let player = this.state.players[this.state.atPlayer]
    this.emit('track:next', player.playing)
  }

  createElement(state,emit){
    this.state = state
    this.emit = emit
    const playing = state.players[state.atPlayer].playing
    return html`
    <div class="flex flex-column h-50 justify-center items-center">
      <h2 class="f3" >${state.userConfig.radio}</h2>
      <div id="player" class="flex flex-row h-30 justify-center center items-center">
        <button class=${this.buttonStyle} onclick=${this.playOrPause}> 
          <i class="fa fa-${playing ? "pause" : "play"} ${this.size}" 
          aria-hidden="true">
          </i>
        </button>
        <button class=${this.buttonStyle} onclick=${this.next}>
          <i class="fa fa-step-forward ${this.size}" aria-hidden="true"></i>
        </button>
      </div>
      <div class="flex flex-row justify-center items-center w-50">
      ${toggle.render(state,emit)}
      <p>Canal 2</p>
      </div>
      <div id="trackInfo" class="flex flex-column h-25 pv4 justify-start center items-center">
        <p class="pa2 b f4 ma1 tc w5"> ${state.currentTrack?.artist || ""}</p>
        <p class="pa2 ma1 f5 tc w6"> ${state.currentTrack?.title || ""}</p>
      </div>
    </div>
    `
  }

  update() {
    return true
  }
}
module.exports = Player
