const Nanocomponent = require('nanocomponent')
const html = require('choo/html')

class Logo extends Nanocomponent {
  constructor(){
    super()
  }
  createElement() {
    return html`
    <div
    class="flex justify-center items-end h-25">
      <img src="logo.png"/>
    </div>

    `
  }

  update() {
    return false
  }

}
module.exports = Logo 
