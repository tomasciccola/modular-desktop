const Nanocomponent = require('nanocomponent')
const html = require('choo/html')
const css = require('sheetify')

const style = css`
:host:focus-visible  {
  outline:1px solid #001b44!important;
}
`

class Login extends Nanocomponent {
  constructor(){
    super()
    this.cellStyle = "flex flex-column mv2"
    this.inputStyle = `pv2 ${style}`
    this.valid = false
    this.fields = {radio:'', licencia:'', cliente:''}
    this.submit = this.submit.bind(this)
  }

  validate() {
    let valid = this.fields['radio'] !== ''
      && this.fields['licencia'] !== ''
      && this.fields['cliente'] !== ''
    this.rerender = valid !== this.valid
    this.valid = valid
  }

  submit(e) {
    e.preventDefault()
    this.emit(this.state.events.LOGIN, e.currentTarget)
  } 

  onChange(field) {
    return e => {
      this.fields[field] = e.target.value
      this.validate()
      this.emit('render')
    }
  }

  createElement(state,emit) {
    this.state = state
    this.emit = emit
    this.fields = state.userConfig
    this.validate()

    return html`
    <form id="login" class="flex flex-column h-50 justify-center" onsubmit=${this.submit}>
      <div class=${this.cellStyle}>
        <label for="radio">Radio</label>
        <input class=${this.inputStyle}
        oninput=${this.onChange('radio')}
        id="radio" name="radio" type="text"
        value=${this.fields['radio']} >
      </div>
      <div class=${this.cellStyle}>
        <label  for="cliente">Cliente</label>
        <input class=${this.inputStyle}
        oninput=${this.onChange('cliente')} 
        id="cliente" name="cliente" type="text"
        value=${this.fields['cliente']} >
      </div>
      <div class=${this.cellStyle}>
        <label for="licencia">Licencia</label>
        <input class=${this.inputStyle} 
        oninput=${this.onChange('licencia')} 
        id="licencia" name="licencia" type="text"
        value=${this.fields['licencia']} >
      </div>
      <input 
        class="${this.valid ? "bg-near-black white" : "silver black"} pv2 glow b--none ${style}"
        type="submit" value="Login">
    </form>
    `
  }

  update(state) {
    return this.rerender 
      || JSON.stringify(state.userConfig) !== JSON.stringify(this.fields)
  }
}
module.exports = Login
