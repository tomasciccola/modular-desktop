const choo = require('choo')
const html = require('choo/html')
const css = require('sheetify')
const app = choo()


css('tachyons')

app.use(require('choo-devtools')())

app.use((state,emitter) => {
  state.events.NEXT = 'next'
  state.events.LOGIN = 'login'
  state.events.SAVE_CONFIG = 'save_config'
  state.events.SYNC = 'sync'

  state.screens = [
    require('./views/login.js'),
    require('./views/sync.js'),
    require('./views/player.js')
  ]
  state.atScreen = 0

  emitter.on(state.events.NEXT, () => {
    state.atScreen += 1
    emitter.emit('render')
  })

})
app.use(require('./stores/login.js'))
app.use(require('./stores/sync.js'))
app.use(require('./stores/player.js'))

const style = css`
:host {
  height:100vh;
}
`

const Footer = require('./components/Footer.js')
const Logo = require('./components/Logo.js')
const footer = new Footer()
const logo = new Logo()

app.route('/', (state,emit) => {
  return html`
  <body class="${style} w-75 black center code overflow-hidden courier">
    ${logo.render()}
    ${state.screens[state.atScreen](state,emit)}
    ${footer.render()}
  </body>
  `
})
app.mount('body')
