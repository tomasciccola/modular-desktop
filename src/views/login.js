const html = require('choo/html')
const Login = require('../components/Login.js')
//const Footer = require('../components/Footer.js')
const login = new Login()

module.exports = (state,emit) => {
  return html`
  ${login.render(state,emit)}
  `
}
