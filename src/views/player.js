const html = require('choo/html')
const Player = require('../components/Player.js')
const player = new Player()
module.exports = (state,emit) => {
  return html`
  ${player.render(state,emit)}
  `
}
