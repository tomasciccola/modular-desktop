const html = require('choo/html')

module.exports = (state,emit) => {
  return html`
  <div class="h-50 flex flex-column justify-center items-center">
    <h2> SYNC </h2>
    <div class="flex flex-row">
      <p>${state.downloadedTracks}/${state.totalTracksToDownload}</p>
    </div>
  </div>
  `
}
