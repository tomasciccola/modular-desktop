const {ipcRenderer:ipc} = require('electron')
const {toSafeFileName} = require('../lib.js')
const path = require('path')
const Player = require('../Player.js')
const pickTrack = require('./trackPicker.js')()

module.exports = (state,emitter) => {
  state.players = [new Player(), new Player()]
  state.atPlayer = 0
  state.currentTrack = null

  //TODO: Do I need altPlayChanged??
  state.altPlayAvailable = false
  state.altPlay = false

  //ipc.send('getMusicPath') //for debugin directly player screen
  emitter.on(state.events.NEXT, ()=> {
    console.log("PANTALLA", state.atScreen)
    if(state.atScreen === 2) {
      ipc.send('getMusicPath')
      state.altPlayAvailable = state.canciones.reduce((_,c) => {
        return c.desde === 'alt' || c.hasta === 'alt'
      }, false)
      emitter.emit('render')
    }
  })

  ipc.on('musicPath', (_,p) => {
    console.log('p', p)
    state.musicPath = p
    emitter.emit('track:load', () => {/*emitter.emit('track:play')*/})
  })

  const aboutToFinishTrack = () => {
    console.log('about to finish track')
    emitter.emit('track:load', () => emitter.emit('track:play', true))
  }

  //player events
  emitter.on('track:load', (loaded) => {
    state.atPlayer = (state.atPlayer + 1) % state.players.length
    const at = state.atPlayer
    const pl = state.players[at]
    const track = pickTrack(state.canciones, state.altPlay)
    const file = path.join(state.musicPath,toSafeFileName(track.url))
    if(track){
      state.currentTrack = track
      pl.load(file,loaded,aboutToFinishTrack)
    }
  })

  emitter.on('track:play', (fadein) => {
    state.players[state.atPlayer].play(fadein)
    emitter.emit('render')
  })

  emitter.on('track:pause', () => {
    state.players[state.atPlayer].pause()
    emitter.emit('render')
  })

  emitter.on('track:next', (autoplay) => {
    emitter.emit('track:pause')
    emitter.emit('track:load', () => {
      if(autoplay) emitter.emit('track:play', false)
    })
    emitter.emit('render')
  })

  emitter.on('altPlay', checked => {
    state.altPlay = checked
    emitter.emit('render')
  })

}
