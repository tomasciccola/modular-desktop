const {ipcRenderer:ipc} = require('electron')

module.exports = (state,emitter) => {
  state.downloadedTracks = 0
  state.totalTracksToDownload = 0
  state.finishDownloadingTracks = false

  emitter.on(state.events.NEXT, ()=> {
    if(state.atScreen === 1){
      emitter.emit(state.events.SYNC)
    }
  })

  emitter.on(state.events.SYNC, () => {
    ipc.send('tracks', JSON.stringify(state.canciones))
  })

  ipc.on('tracksToDownload', (_,n) => {
    state.totalTracksToDownload = n
    emitter.emit('render')
  })

  ipc.on('trackDownloaded', (_,d) => {
    d = JSON.parse(d)
    state.downloadedTracks = d.n
    state.totalTracksToDownload = d.total
    emitter.emit('render')
  })

  ipc.on('tracksDownloaded', () => {
    if(!state.finishDownloadingTracks){
      console.log('done!')
      state.finishDownloadingTracks = true
      emitter.emit(state.events.NEXT)
    }
  })
}
