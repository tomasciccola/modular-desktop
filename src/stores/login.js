const {ipcRenderer:ipc} = require('electron')
const {fetchJson} = require('../lib.js')

module.exports = (state,emitter) => {
  const ROOT_URL = 'http://srz.eu-1.evennode.com'
  state.userConfig = {radio: '', cliente: '', licencia: '', id: ''}
  // get user configuration 
  // {radio, cliente, licencia, id}
  ipc.send('getConfig')
  ipc.on('config', (event,arg) => {
    state.userConfig = arg
    emitter.emit('render')
  })


  emitter.on(state.events.LOGIN, form => {
    let data = new FormData(form)
    let body = {}
    for(let pair of data.entries()) body[pair[0]] = pair[1]
    let c = body.cliente.toLowerCase()
    let l = body.licencia.toLowerCase()
    url = `${ROOT_URL}/${c}/${l}/${state.userConfig.id}`
    fetchJson(url, (err,json) => {
      if(err)return console.log('error', err)
      if(json.licenciaValida){
        state.canciones = json.canciones
        state.userConfig.radio = body.radio
        state.userConfig.cliente = body.cliente
        state.userConfig.licencia = body.licencia
        emitter.emit(state.events.SAVE_CONFIG)
        emitter.emit(state.events.NEXT)
      }else{
        alert('Licencia Inválida')
      }
    })
  })

  emitter.on(state.events.SAVE_CONFIG, () => {
    ipc.send('saveConfig', JSON.stringify(state.userConfig, null, 4))
  })
}
