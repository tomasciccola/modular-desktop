const { DateTime } = require('luxon')

const findValidTracks = altPlay => track =>  {
  if (altPlay) return true

  let ahora = DateTime.now();

  let desdeHoraString = track.desde.split(':')[0]
  let desdeMinutoString = track.desde.split(':')[1]
  let hastaHoraString = track.hasta.split(':')[0]
  let hastaMinutoString = track.hasta.split(':')[1]

  let desde = DateTime.fromObject({hour: desdeHoraString, minute:desdeMinutoString});
  let hasta = DateTime.fromObject({hour: hastaHoraString, minute:hastaMinutoString});

  // si hay un min de diferencia es lo mismo que decir que se reproduzca siempre
  if(Math.abs(desde.hour - hasta.hour) <= 1/60){
    return true
  }

  if(hasta < desde){
    if(ahora < hasta){
      desde = desde.set({day:desde.day - 1})
    }

    if(ahora > hasta){
      hasta = hasta.set({day:hasta.day + 1})
    }
  }

  return (ahora > desde && ahora < hasta)
}

// Obtené un track random que ya no haya salido
const pickRandomTrack = (validTracks,alreadyPlayed) =>  {
  let newTrackIndex = Math.floor(Math.random() * validTracks.length)
  //seleccioná un track
  let newTrack = validTracks[newTrackIndex]
  // recorré los que ya reproducí
  for (let i = 0; i < alreadyPlayed.length; i++) {
    let playedTrack = alreadyPlayed[i]
    // si la url es la misma al que elegí
    if (playedTrack.url === newTrack.url) {
      //volvé a intentarlo
      console.log('ya reproducí este hace poco, reintentando...')
      newTrack = pickRandomTrack(validTracks, alreadyPlayed)
    }
  }
  return newTrack
}


const buildTrackPicker = () => {
  let alreadyPlayedTracks = []

  const pickTrack = (canciones,altPlay) => {

    let validTracks = []
    let track = null

    validTracks = canciones
      .map(track => {
        if (!altPlay && (!track.desde && !track.hasta)) {
          track.desde = "00:00"
          track.hasta = "23:59"
        }
        return track
      }).filter(tr => {
        if (!altPlay) {
          return tr.desde !== "alt" || tr.hasta !== "alt"
        } else {
          return tr.desde === "alt" || tr.hasta === "alt"
        }
      }).filter(findValidTracks(altPlay))

    console.log('n tracks', validTracks.length)
    if (alreadyPlayedTracks.length >= validTracks.length) {
      alreadyPlayedTracks = [] 
      console.log('resetting list')
    }

    // TODO: altPlay changed??
    // if (altPlayChanged) {
    //   alreadyPlayed = []
    //   console.log('resetting list altPlay')
    //   this.setState({altPlayChanged: false})
    // }

    track = pickRandomTrack(validTracks, alreadyPlayedTracks)
    alreadyPlayedTracks.push(track)
    if(!track) alert('no hay tracks disponibles en este horario')
    return track

  }
  return pickTrack

}

module.exports = buildTrackPicker
