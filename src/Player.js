// player
// https://www.html5rocks.com/en/tutorials/webaudio/intro
/*
 load(buffer) --> file.buffer
 play(fadein)
 pause
 */

const fs = window.require('fs')

class Player {
  static ctx = new AudioContext()
  constructor(){
    this.gainNode = Player.ctx.createGain()
    this.init()
  }

  init() {
    this.bufferNode = null
    this.buffer = null // for holding the data
    this.startedAt = 0
    this.pausedAt = 0
    this.playing = false

    this.FADE_TIME = 5 // tiempo de fadeOut
    this.NOTIFY_AT = 10 // cuanto antes te avisa que está por terminar el track
    this.fadeOut = [] // parametros de control para arrancar y terminar el fadeout
    this.finishing = null // el cb para avisar cuando está por terminar el track
    this.finishingTimeout = null // el timeout que corrigo si doy pausa
  }

  reset (){
    setTimeout(() => {
      console.log('finish')
      this.init()
    },this.NOTIFY_AT * 1000)
  }

  load(p, loaded, finishing) {
    fs.readFile(p, (err,f) => {
      if(err) console.log('error cargando archivo!', err) 
      Player.ctx.decodeAudioData(f.buffer, buffer => {
        this.init()
        this.finishing = finishing
        this.buffer = buffer
        loaded()
      })
    })
  }

  play(fadein) {
    fadein  
      ? this.scheduleFadeIn()
      : this.gainNode.gain.exponentialRampToValueAtTime(1, Player.ctx.currentTime)

    let offset = this.pausedAt

    this.bufferNode = Player.ctx.createBufferSource()
    this.bufferNode.connect(this.gainNode)
    this.gainNode.connect(Player.ctx.destination)
    this.bufferNode.buffer =  this.buffer
    this.bufferNode.start(0, offset)

    //timekeeping
    this.startedAt = Player.ctx.currentTime - offset
    this.pausedAt = 0
    this.playing = true

    // fadeout
    let remaining =  this.buffer.duration - offset 
    this.fadeOut = this.scheduleFadeOut()
    this.finishingTimeout = setTimeout(() => {
      this.reset()
      this.finishing()
    }, (remaining - this.NOTIFY_AT) * 1000)

  }

  pause() {
    let elapsed = Player.ctx.currentTime - this.startedAt
    this.stop()
    this.pausedAt = elapsed
    //cancel scheduled FadeOuts
    if(this.fadeOut){
      this.fadeOut.forEach(f => f.cancelScheduledValues(Player.ctx.currentTime))
    }
    this.fadeOut = null
    clearTimeout(this.finishingTimeout)
  }

  stop() {
    if(this.bufferNode){
      this.bufferNode.disconnect()
      this.bufferNode.stop(0)
      //this.bufferNode = null
    }
    this.pausedAt = 0
    this.startedAt = 0
    this.playing = false
  }
  
  scheduleFadeIn() {
    this.gainNode.gain.exponentialRampToValueAtTime(0.0001,Player.ctx.currentTime)
    this.gainNode.gain.exponentialRampToValueAtTime(1, Player.ctx.currentTime + this.FADE_TIME)
  }

  scheduleFadeOut() {
    let fadeTime = this.startedAt + this.buffer.duration
    let initOut = this.gainNode.gain.exponentialRampToValueAtTime(1,fadeTime - this.FADE_TIME)
    let endOut = this.gainNode.gain.exponentialRampToValueAtTime(0.0001,fadeTime)
    return [initOut, endOut]
  }
}

module.exports = Player
