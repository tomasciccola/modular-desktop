const { ipcMain:ipc, BrowserWindow } = require('electron')
const {toSafeFileName} = require('./src/lib.js')
const fs = require('fs')
const downloader  = require('./downloader.js')

const syncCanciones = (canciones,dir) => {
  //const toSafeFileName = n => path.basename(url.parse(n).pathname)
  const possibleDownloads = canciones.map(c => toSafeFileName(c.url))
  const downloadedFiles = fs.readdirSync(dir)

  const needToDownload = possibleDownloads
    .filter(x => !downloadedFiles.includes(x))
    .map(x => canciones.reduce((acc,c) => toSafeFileName(c.url) === x ? c : acc, {}))

  //file deletion
  downloadedFiles
    .filter(x => !possibleDownloads.includes(x)) 
  // .filter((x, idx) => (idx % 2) === 0) // to test deletion
  // .filter(_ => Math.random > 0.5)
    .map(x => canciones.reduce((acc,c) => toSafeFileName(c.url) === x ? c : acc, {}))
    .forEach(f => fs.unlinkSync(`${dir}/${toSafeFileName(f.url)}`))

  return needToDownload
}

const main = (paths) => {

  // CONFIG
  ipc.on('getConfig', (event, _) => {
    let config = JSON.parse(fs.readFileSync(paths.modularConfigFile))
    event.reply('config', config)
  })

  ipc.on('saveConfig', (_,arg) => fs.writeFileSync(paths.modularConfigFile, arg))

  // TRACKS
  ipc.on('tracks', async (event,arg) => {
    const canciones = JSON.parse(arg)
    const needToDownload = syncCanciones(canciones,paths.modularMusicDir)

    console.log('to download', needToDownload)
    event.reply('tracksToDownload', needToDownload.length)
    const urls  = needToDownload.map(c => c.url)

    // no need to download anything
    if(urls.length === 0) {
      event.reply('tracksDownloaded','')
      return
    }

    console.log('gonna download some data')
    const onTrackDownloaded = (n,total) => {
      event.reply('trackDownloaded',JSON.stringify({n, total}))
    } 
    const onDone = _ => event.reply('tracksDownloaded','')
    downloader(urls, paths.modularMusicDir,onTrackDownloaded,onDone)()
  })

  ipc.on('getMusicPath', (event) => {
    event.reply('musicPath', paths.modularMusicDir)
  }) 
} 

module.exports = main
