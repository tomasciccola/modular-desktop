const { DownloaderHelper } = require('node-downloader-helper');
const path = require('path')
const url = require('url')

const downloader = (urls,filesPath, onUrlDone, onFinish) => {
  const CAP = 5 
  let at = 0
  let nDownloading = 0

  const next = () => {
    nDownloading = nDownloading - 1
    if(at < urls.length - 1){
      at = at + 1
      onUrlDone(at,urls.length)
      download()
    }else{
      onFinish()
    }
  }

  const download = () => {
    const fileName = path.basename(url.parse(urls[at]).pathname)
    const dl = new DownloaderHelper(urls[at], filesPath, {retry:true,fileName}) 
    nDownloading = nDownloading + 1
    dl.on('end', next)
    dl.start()
    if(nDownloading < CAP){
      at = at + 1
      download()
    }
  }

  return () => download()
}

module.exports = downloader
