const { app, BrowserWindow } = require('electron')
const fs = require('fs')
const path = require('path')
const { v4: uuid } = require('uuid')
let win
const createWindow = () => {
  win = new BrowserWindow({
    width:500,
    height:800,
    autoHideMenuBar: true, 
    webPreferences: {
      contextIsolation:false,
      nodeIntegration:true
    }
  })
  win.loadFile('public/index.html')
}

let modularDataDir = app.getPath('userData') 
let modularConfigFile = path.join(modularDataDir,'config.json')
let modularMusicDir = path.join(modularDataDir,'music')
// let modularMusicDir = path.join(__dirname, 'src/assets','music')

const resolveAppData = () => {
  const exists = fs.existsSync(modularDataDir)
  if(!exists) {
    fs.mkdirSync(modularDataDir)
    fs.writeFileSync(modularConfigFile, JSON.stringify({}, null, 4))
  }else{
    const configExists = fs.existsSync(modularConfigFile)
    const musicDirExists = fs.existsSync(modularMusicDir)
    if(!configExists){
      let config = {radio: '', licencia: '', cliente: '', id: uuid()}
      fs.writeFileSync(modularConfigFile, JSON.stringify(config), null,4)
    }
    if(!musicDirExists){
      fs.mkdirSync(modularMusicDir)
    }
  }
}

resolveAppData()
require('./ipc.js')({modularConfigFile, modularMusicDir})
app.whenReady().then(createWindow)
